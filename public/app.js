'use strict';
async function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch
  const response = await fetch('/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(userInfo)
  });

  if (response.ok) {
    const addedUser = await response.json();
    //Тоже самое что и Promise.resolve
    return addedUser.id;
  } else {
    return Promise.reject(`Код ошибки:${response.status} ${response.statusText}`);
  }
}

async function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
  const response = await fetch(`/users?id=${id}`);

  if (response.ok) {
    return await response.json();
  } else {
    return Promise.reject(`Код ошибки:${response.status} ${response.statusText}`);
  }
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/users', true);
    xhr.setRequestHeader("Content-type", "application/json;charset=utf-8");
    xhr.onload = async () => {
      if (xhr.status >= 200 && xhr.status < 300) {
        const response = xhr.response;
        const addedUser = JSON.parse(response);
        resolve(addedUser.id);
      } else {
        reject(`Код ошибки:${xhr.status} ${xhr.statusText}`);
      }
    };
    xhr.onerror = () => reject(`Код ошибки:${xhr.status} ${xhr.statusText}`);
    xhr.send(JSON.stringify(userInfo));
  });
}

function getUserXHR(id, callback) {
  // напишите GET-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `/users?id=${id}`, true);
    xhr.onload = () => {
      if (xhr.status >= 200 && xhr.status < 300) {
        const response = xhr.response;
        const json = JSON.parse(response);
        resolve(json);
      } else {
        reject(`Код ошибки:${xhr.status} ${xhr.statusText}`);
      }
    };
    xhr.onerror = () => reject(`Код ошибки:${xhr.status} ${xhr.statusText}`);
    xhr.send(null);
  });
}

function checkWork() {
  //FETCH 
  // addUserFetch({ name: "Alice", lastname: "FetchAPI" })
  //   .then(userId => {
  //     console.log(`Был добавлен пользователь с userId ${userId}`);
  //     return getUserFetch(userId);
  //   })
  //   .then(userInfo => {
  //     console.log(`Был получен пользователь:`, userInfo);
  //   })
  //   .catch(error => {
  //     console.error(error);
  //   });

  addUserXHR({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

checkWork();